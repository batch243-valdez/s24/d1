console.log("Hello!");

// [Section] Exponent operator;

// before ES6
	const firstNum = 8 ** 2;
	console.log(firstNum);

// ES6
	const secondNum = Math.pow(8, 2);
	console.log(secondNum);

// [Section] Template Literals
	/*
		- Allows to write string without using concatenation operator(+)
		- Greatly helps with code readability
	*/

let name = "John";
	// before template literal string
	// Use single quote or double quote
	let message = "Hello " + name + "! Welcome to programming!";
	console.log(message);

	// Using template literal
	// using backties(``);
	message = 
`Hello 
${name}!
Welcome 
to 
programming!`
	console.log(message);

	// Template literals allow us to write strings with embedded JavaScript

	const interestRate = 0.1;
	const principal = 1000;
	console.log(`The interest on your savings account is: ${interestRate * principal}`)

// [Section] Array Destructuring
	/*
		- Allows us to unpack elements in arrays into distinct variables
		- Allows us to name array elements with variables instead of using index numbers.
		- It will help us with code readability
		syntax:
		let/const [variableNameA, VariableNameB, . . .] = arrayName;
	*/

	const fullName = ["Juan", "Dela", "Cruz"];
	// Before array destructuring
	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);
	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`)

	// Array destructuring

	const [firstName, middleName, lastName] = fullName;
	console.log(firstName);
	console.log(middleName);
	console.log(lastName);

	console.log(fullName);
	console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`)

// [Section] Object Destructuring
	/*
		- Allows us to unpact properties of objects into distinct variables
		syntax:
			let/const { propertyNameA, propertyNameB, propertyNameC, . . .} = objectName;
	*/

	const person = {
		givenName: "Jane",
		maidenName: "Dela",
		familyName: "Cruz"
	};

	// Before the object destructuring
		console.log(person.givenName);
		console.log(person.maidenName);
		console.log(person.familyName);
		let {givenName, maidenName, familyName} = person;
		console.log(givenName);
		console.log(maidenName);
		console.log(familyName);

	function getFullName({givenName, maidenName, familyName}){
		console.log(`${givenName} ${maidenName} ${familyName}`)
	}

	getFullName(person);

// [Section] Arrow Functions
	// Compact alternative syntax to traditional functions.
	// Useful for code snippet where creating functions will not be reused in any other portion of the code

	// New
	const hello = () => console.log("Hello World!");
	
	// Before 
	function hello2(){
		console.log("Hello World!");
	}

	hello();
	hello2();

	// Before pre-arrow function and template literals
	function printFullName(firstName, middleName, lastName){
		console.log(firstName + " " + middleName + " " + lastName)
	}
	printFullName("john bazil", "milan", "valdez");

	let fName = (firstName, middleName, lastName) => console.log(`${firstName} ${middleName} ${lastName}`)

	fName("john bazil", "milan", "valdez");

	// Arrow functions with loops

	const student = ["John", "Jane", "judy"];

	// before arrow function

	function iterate(student){
		console.log(student+ " is a student!");
	}
	student.forEach(iterate);

	// Arrow Function

	student.forEach(student => {
		console.log(`${student} is a student!`)
	});
	
	// [Section] Implicit Return Statement
		/*
			- There are instances when you can ommit return statement
			- This works because even without return statement Javascript implicitly adds it for the result of the function
		*/
	
	const add = (x,y) => {
		console.log(x+y);
		return x+y;
	}

	let sum = add(23, 45);
	console.log(sum);

	const subtract = (x,y) => x-y;

	subtract(10,5);
	let difference = subtract(10, 5);
	console.log(difference);

	// [Section] Default Function Argument Vallue
	// provide a default argument value if none is provided when the function is invoked.

	const greet = (name = "Human") =>{
		return `Good morning, ${name}!`;
	}

	console.log(greet());


















